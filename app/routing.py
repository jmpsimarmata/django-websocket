# chat/routing.py
from django.urls import re_path

from app.consumers import EWalletConsumer

websocket_urlpatterns = [
    re_path(r'^ws/ewallet/(?P<username>[^/]+)/$', EWalletConsumer),
]
