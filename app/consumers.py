from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from app.models import TopUpBuktiPembayaran, EWallet
from app.views import get_ewallet_by_username
class EWalletConsumer(WebsocketConsumer):

     def send_message(self, message):
          self.send(text_data=json.dumps(message))

     def fetch_ewallet(self, data):
          e_wallet = get_ewallet_by_username(data['username'])
          content = {
               'command': 'get_ewallet_by_username',
               'ewallet': e_wallet
          }
          self.send_message(content)

     commands = {
        'fetch_ewallet': fetch_ewallet,
     }

     def connect(self):
          self.username = self.scope['url_route']['kwargs']['username']
          async_to_sync(self.channel_layer.group_add)(
               self.username
          )
          self.accept()

     def disconnect(self, close_code):
          async_to_sync(self.channel_layer.group_discard)(
               self.username
          )

     def receive(self, text_data):
          data = json.loads(text_data)
          self.commands[data['command']](self, data)