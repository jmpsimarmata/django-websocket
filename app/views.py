from django.shortcuts import render
from app.models import EWallet

# Create your views here.
def get_ewallet_by_username(username):
     ewallet = None
     try:
          ewallet = EWallet.objects.get(username=username)
     except EWallet.DoesNotExist:
          ewallet = EWallet.objects.create(
               username=username,
               saldo=0
          )
     return ewallet
