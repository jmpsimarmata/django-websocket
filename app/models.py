from django.db import models
from cloudinary.models import CloudinaryField

class EWallet(models.Model):
     username = models.CharField(max_length=255, primary_key=True)
     saldo = models.IntegerField()

     def __str__(self) -> str:
         return self.username + ' - ' + str(self.saldo)

class TopUpBuktiPembayaran(models.Model):
     username = models.CharField(max_length=255)
     nominal = models.IntegerField()
     image = CloudinaryField('image')